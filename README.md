# Setup
## Support
### Currently supported OSs:
 - Windows 
 - Linux

Coming soon:
 - macOS

### Currently supported email providers:
 - GMail

Coming soon:
 - Vivaldi Webmail
 - ProtonMail
 - Tutanota
 - Outlook
 - Amazon Workmail
 - Yahoo! Mail

## Install required items
Install [Python](https://pip.pypa.io/en/stable/)

[cd](https://www.digitalcitizen.life/command-prompt-how-use-basic-commands/) into the directory you downloaded the project files into and run the following command:
```cmd
pip install -r requirements.txt
```


### Optional items
Install [Thunderbird](https://www.thunderbird.net/) and configure it with your appropriate email provider.


## Save your emails
Create a folder inside the directory you downloaded the project files into named "Spam Emails". Put the .eml files you wish to parse inside this folder.


### Thunderbird
If you are using thunderbird, perform the following:
 - navigate to the appropriate folder
 - Select all > Right click > Save as... 
 - (Choose the "Spam Emails" folder you just made)


# Run the program
cd into the directory you downloaded the project files into and run the following command:
```cmd
python main.py
```

# Support
If you would like support for your email provider, found an issue, or have any questions regarding this project, please submit them using the [form found here](https://tizeka.io/contact).

# Changelog
### 14 MAY 2021 v0.01.05a
 - Removed CentralNic RDAP query from code to prevent abuse. Will revisit whole RDAP situation when possible
### 8 MAY 2021 v0.01.05
 - Mitigated issue where Verisign would fail to provide WHOIS data by using CentralNic. NOTE: CentralNic rate limits to 240 requests/hour
 - Attempted to increase speed of 'heart beat' check by pinging site rather than doing a get request. This may or may not be a good idea in the long run. Ping timeout is .5 seconds
 - Removed verbose results, placed results into text files
### 28 MAR 2021 v0.01.04
 - Fixed file transfer issue that would occur when multiple domains referenced the same IP
 - Preparing for message-id reporting 
### 17 MAR 2021 v0.01.03
 - Made date recording function in the SQL DB go off of the .eml file's received date rather than the date thrown on the filename by Thunderbird. Allows for direct downloading of .emls from sites like GML rather than depending on Thunderbird.
 - Preparing for message-id reporting
 - Rolled back gitlab check because although "it's cool" it doesn't really make much sense at this time
### 17 MAR 2021 v0.01.02
 - Made date recording function in the SQL DB go off of the .eml file's received date rather than the date thrown on the filename by Thunderbird. Allows for direct downloading of .emls from sites like GML rather than depending on Thunderbird
 - Some hosts don't like being sent .emls, made it so the program converts the files to .txts
 - Added a gitlab check in the program so users are notified if there are new updates ready for download
### 16 MAR 2021 v0.01.01
 - When zipping .eml files, unzipping can result in an error due to filename length. This was remedied by hashing the filenames
### 15 MAR 2021 v0.01.00
 - Publicized code on git
 - Moved from a dictionary structure to a SQL DB for better querying and less confusion
 - Updated the program to only query for alive IPs
