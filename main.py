import datetime
import json
import eml_parser
import os
import http.client
import time
import requests
import cursor
import hashlib
import subprocess
import threading
from termcolor import colored

from sys import platform
from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker
from database import Emails, Base


''' Databases sourced:
    1- North America - https://www.arin.net/
    2- Europe - https://apps.db.ripe.net/db-web-ui/query
    3- Asia - http://wq.apnic.net/apnic-bin/whois.pl
    4- Latin America - http://lacnic.net/cgi-bin/lacnic/whois
    5- Africa - www.afrinic.net/fr/services/whois-query
'''


'''
    RIPE
    Get Abuse # -> https://rest.db.ripe.net/search.json?query-string={%s} ip
    objects, object, 1, attributes, attribute, 10, value
    https://rest.db.ripe.net/ripe/role/{%s}.json value
    if json contains "ERROR" 
'''


'''
    id = Column(Integer, primary_key=True)
    email = Column(String(250), nullable=False)
    domain_name = Column(String(40), nullable=False)
    ip_address = Column(String(15), nullable=False)
    abuse_contact = Column(String(50), nullable=False)
'''

engine = create_engine('sqlite:///email_database.db', connect_args={'check_same_thread': False})
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()


bln_graphics = True


def notice():

    print("***********************************************************"
          "\n*                         NOTICE                          *"
          "\n* This program is still in development. The following RIR *"
          "\n* DBs have not been implemented at this time:             *"
          "\n*  - AFRINIC                                              *"
          "\n*  - APNIC                                                *"
          "\n*  - LACNIC                                               *"
          "\n*                                                         *"
          "\n* This program only supports GMail users at this time.    *"
          "\n* Visit https://tizeka.io/contact to request support for  *"
          "\n* additional email providers.                             *"
          "\n*                                                         *"
          "\n***********************************************************\n")


    if "darwin" in platform:
        print("This program is not available for macOS yet")
        exit(0)
    elif "win32" in platform or "linux" in platform:
        '''result = input("Fancy-schmancy graphics?(Y/N) ")
        if result.lower() == "yes" or result.lower() == 'y':
            bln_graphics = True
        else:
            bln_graphics = False'''
        # required for windows os.system('color')
        fetch_files()


# Check to see if this is the latest version
'''def latest_update_check():
    str_response = requests.get(
        "https://gitlab.com/tizeka/Spam-Email-Parser/-/refs/master/logs_tree/?format=json",
        headers={'Accept': 'application/json'})
    str_response = json.loads(str_response.content.decode('UTF-8'))
    str_commit = ((str_response[1])['commit'])['id']

    #Change commit number
    if str_commit != "d742f173f7bd331eae63e737e7755e23b60a1352":
        print("A newer version of this program is ready to download at https://gitlab.com/tizeka/Spam-Email-Parser/")
    else:
        notice()'''


def progress(current, total, title, special=None):
    int_select = (current + 9) % 4

    if current != total:
        if 'heart beats' in title and special == 0:
            print('%s %s/%s %s' % (title, current, total, '❤'), end="\r")
            return
        elif 'Querying' in title and special == 0:
            # arr_item = ['-  ', ' - ', '  -', ' - ']
            # arr_item[int_select]
            print('%s %s/%s %s' % (title, current, total, ' '), end="\r")
            return
        else:
            print('%s %s/%s %s' % (title, current, total, ' '), end="\r")
            return
    print('%s %s/%s %s' % (title, current, total, '   '), end="\n")
    return


def fetch_files():
        lst_found = []
        int_count = 0
    
        str_pth_folder = os.path.join(os.getcwd(), "Spam Emails")
    
        # Ask user if they want files with DOA IPs to be deleted or not
            # Ask user if they want DOA IPs to be shown or not
        # Ask users if they want to check to if the URLs connected DOA IPs are connected to a different IP
    
        # Ask user what email provider they have
        # Ask user what email client they're using
    
        # If the table exists (premature termination from previous run), delete
        Emails.__table__.drop(engine)
        Emails.__table__.create(engine)
    
        cursor.hide()
        for file in os.listdir(str_pth_folder):
            if file.lower().endswith(".eml"):
                str_file_hash = hashlib.md5(file.encode("UTF-8")).hexdigest() + ".txt"
                os.rename(os.path.join(str_pth_folder, file), os.path.join(str_pth_folder, str_file_hash))
                session.add(Emails(email=str_file_hash))
                int_count = int_count + 1

        if int_count == 0:
            print("No emails were found to inspect")
            print("Terminating session...")
            return

        session.commit()
        print("%s emails found to inspect" % int_count)

        # Returns a dictionary of emails to IPs and an array of IPs to URLs
        read_files(int_count)

        int_records = session.query(Emails.ip_address).distinct().count()
        print("%s unique IPs found" % int_records)

        int_message_ids = session.query(Emails.message_id, Emails.status_code).distinct().count()
        if int_message_ids > 0:
            print("%s unique Message IDs found" % int_message_ids)

        # Check for IP heartbeats
        check_ip_pulse(int_records)
        int_doa_records = int_records
        # IP addresses can have multiple domains pointing at them, source of some errors
        int_records = session.query(Emails.ip_address, Emails.status_code).distinct().filter(Emails.status_code == '200').count()
        int_doa_records = int_doa_records - int_records
        if int_doa_records > 0 and int_records > 0:
            print("%s IPs still alive, %s IPs DOA" % (int_records, int_doa_records))
        elif int_records == 0:
            print("All IPs DOA... Terminating session")
            return
        else:
            print("All IP still alive")

        # Maybe include URL heartbeats?? for users who want to go down the rabbit hole like me
        # Note: because I forgot why I wrote this comment lol, URLs may change IPs (e.g. mobile32pin.com changed IPs twice)

        # Query abuse contacts for alive IPs, be mindful of multiple domains
        # See if duplicate records might exist
        int_duplicate_records = session.query(Emails.domain_url, Emails.status_code).distinct().filter(Emails.status_code == '200').count()
        if int_duplicate_records > int_records:
            print("Multiple domains found referencing the same IPs")
            query_RIR_databases(int_duplicate_records)
        else:
            query_RIR_databases(int_records)

        print("\n##################################")
        print("#          MESSAGE IDS           #")
        print("##################################\n")
        message_ids()
        # Generate guidance for where emails should be sent off to
        print("\n##################################")
        print("#             HOSTS              #")
        print("##################################\n")
        hosting_contacts()

        # Generate guidance for where emails should be sent off to
        print("\n##################################")
        print("#           REGISTRARS           #")
        print("##################################\n")
        registrar_contacts()

        # Delete database
        db_file = os.path.join(os.getcwd(), 'email_database.db')
        os.remove(db_file)

        cursor.show()


#Make this into its own object??
def check_ip_pulse(int_records):
    threads = []
    str_response = 1
    int_current = 0
    for record in session.query(Emails.ip_address).distinct():
        try:
            # Timeout might be too short
            str_response = subprocess.run("ping -c 1 -W .5 %s" % record.ip_address, shell=True, check=True, stdout=subprocess.DEVNULL).returncode
        except:
            str_response = 1

        if str_response == 0:
            session.query(Emails).filter(Emails.ip_address == record.ip_address).update(
                {
                    "status_code": 200
                }
            )
        int_current = int_current + 1
        progress(int_current, int_records, "Detecting heart beats...", str_response)
    session.commit()



def message_ids():
    print("\n")
    for record in session.query(Emails.message_id, Emails.status_code).distinct().filter(Emails.status_code == '200'):
        print(record.message_id)


def registrar_contacts():
    arr_registrar_contacts = []
    for record in session.query(Emails.registrar_contact).distinct():
        arr_registrar_contacts.append(record.registrar_contact)

    for registrar_contact in arr_registrar_contacts:
        records = session.query(Emails.registrar_contact, Emails.ip_address, Emails.domain_url).distinct().filter(Emails.registrar_contact == registrar_contact)

        if registrar_contact is not None:
            print("\n\n################################## - %s\n" % registrar_contact)
            for record in records:
                print(record.domain_url + " (" + record.ip_address + ")", end='\n')


def hosting_contacts():
    arr_hosting_contacts = []
    for record in session.query(Emails.hosting_contact).distinct():
        arr_hosting_contacts.append(record.hosting_contact)

    for hosting_contact in arr_hosting_contacts:
        records = session.query(Emails.hosting_contact, Emails.ip_address, Emails.domain_url).distinct().filter(Emails.hosting_contact == hosting_contact)

        if hosting_contact is not None:
            print("\n\n################################## - %s\n" % hosting_contact)
            for record in records:
                print(record.domain_url + " (" + record.ip_address + ")", end='\n')

            # Rather than movine emails based on IP or Domain (which might cause issues due to IPs and Domains changing)
            # Just send all related emails to the appropriate folder
            emails = session.query(Emails.email, Emails.hosting_contact).filter(Emails.hosting_contact == hosting_contact)
            for email in emails:
                move_files(email.email, hosting_contact)

        else:
            emails = session.query(Emails.email, Emails.hosting_contact).filter(Emails.hosting_contact == None)
            for email in emails:
                move_files(email.email, "@Dead On Arrival")


def move_files(str_filename, str_company):
    str_pth_folder_cur = os.path.join(os.getcwd(), "Spam Emails")
    str_company = (str_company[str_company.find("@") + 1:])
    str_pth_folder_new = os.path.join(str_pth_folder_cur, str_company)

    str_file_cur_loc = os.path.join(str_pth_folder_cur, str_filename)
    str_file_new_loc = os.path.join(str_pth_folder_new, str_filename)

    if not os.path.exists(str_pth_folder_new):
        os.makedirs(str_pth_folder_new)

    os.replace(str_file_cur_loc, str_file_new_loc)


#returns a dictionary of IPs to emails and a dictionary of IPs to URLs
def read_files(int_count):
    #fancy graphics because why not
    str_record_a = ""
    str_record_b = ""
    str_record_c = ""

    int_current = 0
    str_pth_folder = os.path.join(os.getcwd(), "Spam Emails")

    for record in session.query(Emails):
        arr_file_info = parse_file(os.path.join(str_pth_folder, record.email))
        #synchronize_session = False
        #add
        session.query(Emails).filter(Emails.email == record.email).update(
            {
                "ip_address": arr_file_info[0],
                "domain_url": arr_file_info[1],
                "date_sent": arr_file_info[2],
                "message_id": arr_file_info[3],
                "status_code": 404
            }
        )

        int_current = int_current + 1
        '''if bln_graphics == True:
            #Fancy graphics time woooo
            if str_record_a == "":
                str_record_a = record.email[:record.email.find(".")]
            elif str_record_b == "":
                str_record_b = record.email[:record.email.find(".")]
            elif str_record_c == "":
                str_record_c = record.email[:record.email.find(".")]
                print(str_record_a + " " + str_record_b + " " + str_record_c)
                str_record_a = ""
                str_record_b = ""
                str_record_c = ""

            if int_current == int_count and str_record_c == None:
                print(str_record_a + " " + str_record_b + " " + str_record_c)
            elif int_current == int_count:
                #Fix this later
                print("\n")'''

        progress(int_current, int_count, "Reading email headers...", 0)
    session.commit()

    return


def parse_file(str_eml_file):
    arr_file_info = []
    with open(str_eml_file, 'rb') as fhdl:
        raw_email = fhdl.read()

    ep = eml_parser.EmlParser()
    str_parsed_eml = ep.decode_email_bytes(raw_email)

    str_jsond_eml = json.loads(json.dumps(str_parsed_eml, default=json_serial))

    # we traverse the header to get to the received information.
    # google

    # Does the message ID reveal who the sender is? Clean up the domain
    str_from_id = ((((str_jsond_eml['header'])['header'])['message-id'])[0]).replace('<', '').replace('>', '')
    str_from_id = str_from_id[str_from_id.find("@")+1:]
    # if str_from_id.count(".") > 1:
    #    str_from_id = str_from_id[str_from_id[:str_from_id.rfind(".")].rfind(".")+1:]

    str_from = (((str_jsond_eml['header'])['received'])[1])

    str_from_src = str_from['src']
    str_from_date = str_from['date']

    # Fetch Domain and IP
    # Notice: Not all emails have 'aliases'
    # Notice: Some emails have multiple 'aliases'. This may be worth investigating
    if ". [" in str_from_src:
        str_from_src = str_from_src[str_from_src.find("(")+1:str_from_src.find(")")]
        str_from_ip = str_from_src[str_from_src.find("[")+1:str_from_src.find("]")]
        str_from_url = str_from_src[:str_from_src.find("[")-2]

    else:
        str_from_ip = str_from_src[str_from_src.find("[")+1:str_from_src.find("]")]
        str_from_url = str_from_src[str_from_src.find(" ")+1:str_from_src.find("([")-1]

    # Fetch Date
    str_from_date = str_from_date[:str_from_date.rfind("-")].replace('T', ' ')
    str_from_date = datetime.datetime.strptime(str_from_date, '%Y-%m-%d %H:%M:%S')

    arr_file_info.append(str_from_ip)
    arr_file_info.append(str_from_url)
    arr_file_info.append(str_from_date)
    arr_file_info.append(str_from_id)

    return arr_file_info


def json_serial(obj):
    if isinstance(obj, datetime.datetime):
        serial = obj.isoformat()
        return serial


# This section could be an object... maybe?
def query_RIR_databases(int_records):
    int_current = 0

    for record in session.query(Emails.ip_address, Emails.domain_url, Emails.status_code).distinct().filter(Emails.status_code == '200'):
        str_hosting_contact = fetch_ip_info(record.ip_address)
        str_registrar_contact = fetch_domain_info(record.domain_url)
        session.query(Emails).filter(Emails.ip_address == record.ip_address).update(
            {
                "hosting_contact": str_hosting_contact,
                "registrar_contact": str_registrar_contact
            }
        )
        int_current = int_current + 1
        progress(int_current, int_records, "Querying RIR and ICANN databases...", 0)
    session.commit()

def fetch_domain_info(str_url):
    try:
        # https://www.icann.org/en/system/files/files/rdap-pilot-report-25apr19-en.pdf
        # Try
        response = requests.get("https://rdap.verisign.com/com/v1/domain/%s" % str_url,
                                    headers={'Accept': 'application/json'})

        if response.status_code == 200:
            str_response = json.loads(response.content.decode('UTF-8'))
            str_allocation = ((((((((str_response['entities'])[0])['entities'])[0])['vcardArray'])[1])[3])[3]).upper()
            return str_allocation

        # ICANN uses this, but it's rate limited. Last Resort.
        # Removed because permanent ban if rate limit exceeded. Looking into alternative solutions.
        '''response = requests.get("https://rdap.centralnic.com/online/domain/%s" % str_url,
                                headers={'Accept': 'application/json'}, timeout=3)

        if response.status_code == 200:
            str_response = json.loads(response.content.decode('UTF-8'))
            str_allocation = ((((((((str_response['entities'])[4])['entities'])[0])['vcardArray'])[1])[3])[3]).upper()
            return str_allocation'''

        # https://apis.cscglobal.com/dbs/rdap-api/v1/domain/

        str_allocation = "Whois Unavailable"
        return str_allocation

    except:
        str_allocation = "WHOIS Unavailable"
    return str_allocation

def fetch_ip_info(str_ip):
    str_abuseemail = fetch_ARIN(str_ip)
    return str_abuseemail


# ARIN tells us what NIC we can find the IP's info at, if it's not in the ARIN db. We'll start with ARIN.
def fetch_ARIN(str_ip):
    str_response = requests.get("http://whois.arin.net/rest/ip/%s" % str_ip, headers={'Accept': 'application/json'})
    str_response = json.loads(str_response.content.decode('UTF-8'))
    str_allocation = ((str_response['net'])['orgRef'])['@handle'].upper()

    # If the allocation is a NIC, query the NIC, otherwise note the appropriate host details
    if str_allocation == 'RIPE':
        str_abuseemail = fetch_RIPE(str_ip)
    elif str_allocation == 'APNIC':
        str_abuseemail = fetch_APNIC(str_ip)
    elif str_allocation == 'LACNIC':
        str_abuseemail = fetch_AFRINIC(str_ip)
    elif str_allocation == 'AFRINIC':
        str_abuseemail = fetch_LACNIC(str_ip)
    else:
        str_response = requests.get("https://rdap.arin.net/registry/ip/%s" % str_ip, headers={'Accept': 'application/json'})
        str_response = json.loads(str_response.content.decode('UTF-8'))
        try:
            str_abuseemail = ((((((((str_response['entities'])[0])['entities'])[2])['vcardArray'])[1])[5])[3]).upper()
        except:
            str_abuseemail = ((((((((str_response['entities'])[0])['entities'])[0])['vcardArray'])[1])[5])[3]).upper()

    return str_abuseemail


def fetch_RIPE(str_ip):
    str_response = requests.get("https://stat.ripe.net/data/abuse-contact-finder/data.json?resource=%s" % str_ip)
    str_response = json.loads(str_response.content.decode('UTF-8'))

    str_abuseemail = ((((str_response['data'])['anti_abuse_contacts'])['abuse_c'])[0])['email'].upper()

    return str_abuseemail


def fetch_APNIC(str_ip):
    conn = http.client.HTTPConnection('ip-api.com')
    conn.request("GET", "/json/%s" % str_ip)
    str_abuseemail = ''
    return str_abuseemail


def fetch_LACNIC(str_ip):
    conn = http.client.HTTPConnection('ip-api.com')
    conn.request("GET", "/json/%s" % str_ip)
    str_abuseemail = ''
    return str_abuseemail


def fetch_AFRINIC(str_ip):
    conn = http.client.HTTPConnection('ip-api.com')
    conn.request("GET", "/json/%s" % str_ip)
    str_abuseemail = ''
    return str_abuseemail


notice()